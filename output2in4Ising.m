function [h,J,en,variableNames] = output2in4Ising(inputFile)
    %% convert a 2-in-4-SAT CSP problem (in .bench format)
    %% into an Ising model

    % read the bench file:
    [constraints,variableNames] = read2in4BenchFile(inputFile);

    % 2-in-4 template penalty model:
    model.h = zeros(6,1);                       % [h,J] ising model
    model.J = zeros(6); 
    model.J(1:3,4:6) = [1,1,-1;1,1,1;1,-1,0];
    model.en = -4;                              % ground energy
    model.g = 2;                                % feasible/infeasible energy gap
    model.vp = [1,5,4,2];                       % variable placement

    % generate an ising model for the full problem:
    [h,J,en] = buildIsingModel(constraints,model);

    % output the Ising model to a file:
    outputFile = [inputFile(1:end-5),'ising'];
    exportToIsingFile(outputFile,h,J,en)
end

function [h,J,en] = buildIsingModel(constraints,model)
    %% given a 2-in-4 sat problem (constraints) 
    %% and a penalty model representing a 2-in-4-sat constraint (model),
    %% generate an ising model [h,J] representing the entire problem.
    
    % penalty mdoel with variables in the correct order:
    numAuxVars = length(model.h) - length(model.vp);
    varOrder = [model.vp,setdiff(1:length(model.J),model.vp)];
    h1 = model.h(varOrder);
    J1 = model.J(varOrder,varOrder);    
    
    
    numVars = max([constraints{:}]);
    numConstraints = length(constraints);
    totalVars = numVars + numConstraints*numAuxVars;
    
    % build the model, constraint-by-constraint
    c = numVars;
    h = sparse(totalVars,1);
    J = sparse(totalVars,totalVars);
    for i = 1:numConstraints
        auxScope = c+(1:numAuxVars);
        s = [constraints{i},auxScope];
        h(s) = h(s) + h1;
        J(s,s) = J(s,s) + J1;          
        c = c+numAuxVars;
    end
    
    % convert J to upper triangular:
    J = triu(J,1) + tril(J,-1)';
    
    % ground state energy of the full model:
    en = numConstraints*model.en;
end  

function [constraints,varNames] = read2in4BenchFile(fileName)
%% Read 2-in-4-SAT from a Bench file.

    fid = fopen(fileName, 'r');
    if fid == -1
       error('opening file: %s', message);
    end
    f = 1;
    
    constraints = cell(0);
    varNames = cell(0);
    tline = fgetl(fid);
    while ischar(tline)

        if isempty(tline) || ~isempty(regexp(tline, '^\s*$', 'once'))
            % blank line
            
        elseif regexp(tline, '^\s*#')
            % comment
            
        elseif regexp(tline, '^\s*[a-z,A-Z,\[]')
            % remove whitespace
            tline(tline==' ') = '';
            
            % parse equation line
            tmp = cell(0);
            c = 1; % variable count

            % parse function name
            idx = strfind(tline,'(');
            type = tline(1:idx-1);
            tline = tline(idx+1:end);
            
            % parse input variables
            idx = strfind(tline,',');
            while idx
                tmp{c} = tline(1:idx-1);
                c = c+1;
                tline = tline(idx+1:end);
                idx = strfind(tline,',');
            end
            idx = strfind(tline,')');
            tmp{c} = tline(1:idx-1);
            
            % put input variables first
            nv = length(tmp);
            tmp = tmp([2:nv,1]);
            
            % find variable indices
            varIndex = zeros(1,nv);
            for i = 1:nv 
                varIndex(i) = findVarIndex(varNames,tmp{i});
                if ~varIndex(i)
                    varIndex(i) = length(varNames)+1;
                    varNames = [varNames;tmp{i}];
                end
            end
            
            % find constraint type
            if strcmp(type,'TWOINFOUR')
                constraints{f} = varIndex;
                f = f+1;
            else
                error('unidentified line');
            end
        end

        tline = fgetl(fid);
    end
    fclose(fid);
    
    
    function idx = findVarIndex(varNames,name)
        idx = 0;
        j = 1;
        while j <= length(varNames)
            if strcmp(varNames{j},name)
                idx = j;
                j = length(varNames)+1;
            end
            j = j+1;
        end
    end    
end

function [] = exportToIsingFile(fname,h,J,en)
    %% export the ising model to a file
    %
    % ising files have the following format.
    % first line: (# of qubits) (# of h and J terms)
    % h lines: have the form
    % i i h_i 
    % J lines: have the form
    % i j J_{ij}
    %
    J = triu(J) + tril(J,-1)';
    fs1 = fopen(fname,'w');
    [J1,J2,JW] = find(triu(J,1));
    [hI,hW] = find(h);
    
    fprintf(fs1,'c  FILE: %s\n',fname);
    fprintf(fs1,'c  DESCRIPTION: Ising model representation of a 2-in-4-SAT problem\n');
    fprintf(fs1,'c  SOURCE: https://bitbucket.org/aqcsat/frocos2017\n');
    fprintf(fs1,'c  NOTE: Minimum energy = %d\n',en);
    fprintf(fs1,'c\n');
    fprintf(fs1,'c  This ising file has the following format.\n');
    fprintf(fs1,'c  Lines starting with c are comments to be ignored\n');
    fprintf(fs1,'c  First non-comment line: (# of qubits) (# of h and J terms)\n');
    fprintf(fs1,'c  h lines have the form: i i h_i\n');
    fprintf(fs1,'c  J lines have the form: i j J_{ij}\n');   
    fprintf(fs1,'c\n');    
    % number of qubits, number of h or J terms:
    fprintf(fs1,'%d %d\n',length(h),length(J1)+length(hI));
    
    % h terms:
    for i = 1:length(hI)
        % 0-indexed.
        fprintf(fs1,'%d %d %0.6g\n', hI(i)-1,hI(i)-1,hW(i));
    end
    % J terms:
    for i = 1:length(J1)
        fprintf(fs1,'%d %d %0.6g\n',J1(i)-1,J2(i)-1,JW(i));
    end
    fclose(fs1);

end

  
