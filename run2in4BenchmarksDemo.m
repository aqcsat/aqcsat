function [answer] = run2in4BenchmarksDemo(isingFile,embeddingFile)
    %%
    %% Solve a 2-in-4-SAT problem using a D-Wave processor.
    %%
    % This function requires D-Wave's SAPI package on your MATLAB path
    % as well as access to a D-Wave solver (indicated on lines 17-18).
    %
    % Input: 
    % isingFile: name of a .ising file giving the Ising model to be solved
    % embeddingFile: name of a .embedding file which embeds the Ising model
    %       onto a D-Wave hardware graph.
    %
    % Output:
    % answer: Ising model solutions, energies, occurrences, and timing.

    %% Connect to D-Wave solver
    url = 'https...';                               % access URL provided by D-Wave
    token = '...';                                  % access token provided by D-Wave
    conn = sapiRemoteConnection(url,token);
    solver = sapiSolver(conn, 'DW_2000Q');  
    A = getHardwareAdjacency(solver);               % adjacency matrix of the hardware graph

    %% read in the Ising model  
    [h,J] = readIsing(isingFile);
    
    %% read in the embedding
    emb = importEmb(embeddingFile);
    
    %% set parameters for solving
    sapiParams = struct('num_reads',1000,...        % take 1000 samples from hardware
        'num_spin_reversal_transforms',10,...       % perform spin-reversal transformations
        'postprocess','optimization');              % post-process results for optimization
    sapiParams.chains = emb(cellfun(@length,emb)>1);  % pass embedding to post-processor  
    chain_strength = 1;                             % set strength of embedding couplers

    %% embed ising model
    [embH, embJ, embFMJ] = sapiEmbedProblem(h, J, emb, A);
    fullJ = embJ + chain_strength * embFMJ;         % Ising model including embedding couplers
    
    %% solve Ising model on D-Wave hardware
    answer = sapiSolveIsing(solver, embH, fullJ, sapiParams);

    %% unembed solutions (return solutions to unembeded ising model space)
    answer.solutions = sapiUnembedAnswer(answer.solutions, emb,'discard');
    answer.energies = dot(answer.solutions,J*answer.solutions) + h'*answer.solutions;

    %% output results
    occs = answer.num_occurrences(answer.energies <= min(answer.energies)+1e-6);
    fprintf('%s:\n    Hardware returned %d lowest-energy solutions out of %d samples.\n',isingFile,sum(occs),sapiParams.num_reads);
    fprintf('    Sample lowest-energy solution: ');
    fprintf('%d ',answer.solutions(:,1)); fprintf('\n');
end


function [h,J] = readIsing(fileName)
    % read an Ising file format.
    [fp, message] = fopen(fileName, 'r');
    if fp == -1
       error('opening file: %s', message);
    end;
    c = 0;
    line= fgetl(fp);
    while length(line) ~= 1 || line ~= -1
        if line(1) ~= 'c'
            if ~c
                tmp = textscan(line, '%d %d', 1);
                numVar = double(tmp{1});   numTerms = tmp{2}; 
                M = zeros(numTerms,3);
                c = 1;
            else
                tmp = textscan(line, '%d %d %f', 1);
                M(c,:) = double([tmp{:}]);
                c = c+1;
            end
        end
        line= fgetl(fp);
    end
    fclose(fp);
    Q = sparse(M(:,1)+1,M(:,2)+1,M(:,3), numVar, numVar);
    h = diag(Q);
    J = triu(Q,1);
end


function [emb] = importEmb(fname)
    % read an embedding from a file.
    % line i gives the list of qubits corresponding to variable i.
    [fp, message] = fopen(fname, 'r');
    if fp == -1
       error('opening file: %s', message);
    end;
    emb = cell(0);
    c = 1;
    line= fgetl(fp);
    while length(line) ~= 1 || line ~= -1
        if line(1)~='c' % ignore comments
            tmp = textscan(line, '%d');
            emb{c} = double(tmp{1}');
            c = c+1;
        end
        line= fgetl(fp);
    end
    fclose(fp);
end