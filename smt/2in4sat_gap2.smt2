(set-option :produce-models true)
(set-logic QF_LRA)
;;; model parameter variable definitions
(declare-fun mu1 () Real)
(declare-fun mu2 () Real)
(declare-fun mu3 () Real)
(declare-fun mu4 () Real)
(declare-fun mu5 () Real)
(declare-fun mu6 () Real)
(declare-fun mu7 () Real)
(declare-fun mu8 () Real)
(declare-fun mu9 () Real)
(declare-fun mu10 () Real)
(declare-fun mu11 () Real)
(declare-fun mu12 () Real)
(declare-fun mu13 () Real)
(declare-fun mu14 () Real)
(declare-fun mu15 () Real)
(declare-fun mu16 () Real)
;;; hidden variable definitions
(declare-fun beta1 () Bool)
(declare-fun beta2 () Bool)
(declare-fun beta3 () Bool)
(declare-fun beta4 () Bool)
(declare-fun beta5 () Bool)
(declare-fun beta6 () Bool)
(declare-fun beta7 () Bool)
(declare-fun beta8 () Bool)
(declare-fun beta9 () Bool)
(declare-fun beta10 () Bool)
(declare-fun beta11 () Bool)
(declare-fun beta12 () Bool)
;;; message variable definitions
(declare-fun m1 () Real)
(declare-fun m2 () Real)
(declare-fun m3 () Real)
(declare-fun m4 () Real)
(declare-fun m5 () Real)
(declare-fun m6 () Real)
(declare-fun m7 () Real)
(declare-fun m8 () Real)
(declare-fun m9 () Real)
(declare-fun m12 () Real)
(declare-fun m15 () Real)
(declare-fun m16 () Real)
(declare-fun m17 () Real)
(declare-fun m18 () Real)
(declare-fun m21 () Real)
(declare-fun m24 () Real)
(declare-fun m27 () Real)
(declare-fun m30 () Real)
(declare-fun m33 () Real)
(declare-fun m36 () Real)
(declare-fun m39 () Real)
(declare-fun m42 () Real)
(declare-fun m45 () Real)
(declare-fun m48 () Real)
;;; break gauge symmetry by fixing hidden variables for F(:,1)
(assert (= beta1 false))
(assert (= beta2 false))
(assert (>= (+  (* 2.00 mu6)  (* 2.00 mu12)  (* 2.00 mu13)  (* (- 2.00) mu16) ) 0))
(assert (>= (+  (* 2.00 mu7)  (* (- 2.00) mu14)  (* (- 2.00) mu15)  (* (- 2.00) mu16) ) 0))
;;; bounds constraints
(assert (and (>=  mu2  (- 2.00)) (<=  mu2  2.00)))
(assert (and (>=  mu3  (- 2.00)) (<=  mu3  2.00)))
(assert (and (>=  mu4  (- 2.00)) (<=  mu4  2.00)))
(assert (and (>=  mu5  (- 2.00)) (<=  mu5  2.00)))
(assert (and (>=  mu6  (- 2.00)) (<=  mu6  2.00)))
(assert (and (>=  mu7  (- 2.00)) (<=  mu7  2.00)))
(assert (and (>=  mu8  (- 1.00)) (<=  mu8  1.00)))
(assert (and (>=  mu9  (- 1.00)) (<=  mu9  1.00)))
(assert (and (>=  mu10  (- 1.00)) (<=  mu10  1.00)))
(assert (and (>=  mu11  (- 1.00)) (<=  mu11  1.00)))
(assert (and (>=  mu12  (- 1.00)) (<=  mu12  1.00)))
(assert (and (>=  mu13  (- 1.00)) (<=  mu13  1.00)))
(assert (and (>=  mu14  (- 1.00)) (<=  mu14  1.00)))
(assert (and (>=  mu15  (- 1.00)) (<=  mu15  1.00)))
(assert (and (>=  mu16  (- 1.00)) (<=  mu16  1.00)))
;;; graphs automorphism constraints
;;; penalty P(F(:,1),a)) for x in F
;;; eliminate a(5)
(assert (<= (+ (+  mu6  mu12  mu13  (* (- 1.00) mu16) )  m1 ) 0))
(assert (<= (+ (+  mu6  mu12  mu13  mu16 )  m2 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  (* (- 1.00) mu13)  mu16 )  m1 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m2 ) 0))
;;; make message lower bounds tight
(assert (=>  (and (not beta1)  (not beta2) ) (>= (+ (+  mu6  mu12  mu13  (* (- 1.00) mu16) )  m1 ) 0)))
(assert (=>  (and (not beta1)  beta2 ) (>= (+ (+  mu6  mu12  mu13  mu16 )  m2 ) 0)))
(assert (=>  (and beta1  (not beta2) ) (>= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  (* (- 1.00) mu13)  mu16 )  m1 ) 0)))
(assert (=>  (and beta1  beta2 ) (>= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m2 ) 0)))
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m1)  m3 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  mu15 ) (+  (* (- 1.00) m2)  m3 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta2)  (>= (+ (+  mu7  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m1)  m3 )) 0)))
(assert (=>   beta2  (>= (+ (+  (* (- 1.00) mu7)  mu14  mu15 ) (+  (* (- 1.00) m2)  m3 )) 0)))
;;; require min_a P(F(:,1),a) = 0 
(assert (= (+ (+  mu1  (* (- 1.00) mu2)  (* (- 1.00) mu3)  mu4  mu5  (* (- 1.00) mu8)  (* (- 1.00) mu9)  (* (- 1.00) mu10)  (* (- 1.00) mu11) )  m3 ) 0))
;;; penalty P(F(:,2),a)) for x in F
;;; eliminate a(5)
(assert (<= (+ (+  mu6  (* (- 1.00) mu12)  mu13  (* (- 1.00) mu16) )  m4 ) 0))
(assert (<= (+ (+  mu6  (* (- 1.00) mu12)  mu13  mu16 )  m5 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  mu12  (* (- 1.00) mu13)  mu16 )  m4 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  mu12  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m5 ) 0))
;;; make message lower bounds tight
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m4)  m6 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m5)  m6 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta4)  (>= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m4)  m6 )) 0)))
(assert (=>   beta4  (>= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m5)  m6 )) 0)))
;;; require min_a P(F(:,2),a) = 0 
(assert (= (+ (+  mu1  (* (- 1.00) mu2)  mu3  (* (- 1.00) mu4)  mu5  mu8  (* (- 1.00) mu9)  (* (- 1.00) mu10)  mu11 )  m6 ) 0))
;;; penalty P(F(:,3),a)) for x in F
;;; eliminate a(5)
(assert (<= (+ (+  mu6  mu12  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m7 ) 0))
(assert (<= (+ (+  mu6  mu12  (* (- 1.00) mu13)  mu16 )  m8 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  mu13  mu16 )  m7 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  mu13  (* (- 1.00) mu16) )  m8 ) 0))
;;; make message lower bounds tight
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m7)  m9 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m8)  m9 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta6)  (>= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m7)  m9 )) 0)))
(assert (=>   beta6  (>= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m8)  m9 )) 0)))
;;; require min_a P(F(:,3),a) = 0 
(assert (= (+ (+  mu1  (* (- 1.00) mu2)  mu3  mu4  (* (- 1.00) mu5)  (* (- 1.00) mu8)  mu9  mu10  (* (- 1.00) mu11) )  m9 ) 0))
;;; penalty P(F(:,4),a)) for x in F
;;; eliminate a(5)
;;; make message lower bounds tight
(assert (=> (or  (and (not beta3)  (not beta4) ) (and (not beta7)  (not beta8) )) (>= (+ (+  mu6  (* (- 1.00) mu12)  mu13  (* (- 1.00) mu16) )  m4 ) 0)))
(assert (=> (or  (and (not beta3)  beta4 ) (and (not beta7)  beta8 )) (>= (+ (+  mu6  (* (- 1.00) mu12)  mu13  mu16 )  m5 ) 0)))
(assert (=> (or  (and beta3  (not beta4) ) (and beta7  (not beta8) )) (>= (+ (+  (* (- 1.00) mu6)  mu12  (* (- 1.00) mu13)  mu16 )  m4 ) 0)))
(assert (=> (or  (and beta3  beta4 ) (and beta7  beta8 )) (>= (+ (+  (* (- 1.00) mu6)  mu12  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m5 ) 0)))
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m4)  m12 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m5)  m12 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta8)  (>= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m4)  m12 )) 0)))
(assert (=>   beta8  (>= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m5)  m12 )) 0)))
;;; require min_a P(F(:,4),a) = 0 
(assert (= (+ (+  mu1  mu2  (* (- 1.00) mu3)  (* (- 1.00) mu4)  mu5  (* (- 1.00) mu8)  mu9  mu10  (* (- 1.00) mu11) )  m12 ) 0))
;;; penalty P(F(:,5),a)) for x in F
;;; eliminate a(5)
;;; make message lower bounds tight
(assert (=> (or  (and (not beta5)  (not beta6) ) (and (not beta9)  (not beta10) )) (>= (+ (+  mu6  mu12  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m7 ) 0)))
(assert (=> (or  (and (not beta5)  beta6 ) (and (not beta9)  beta10 )) (>= (+ (+  mu6  mu12  (* (- 1.00) mu13)  mu16 )  m8 ) 0)))
(assert (=> (or  (and beta5  (not beta6) ) (and beta9  (not beta10) )) (>= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  mu13  mu16 )  m7 ) 0)))
(assert (=> (or  (and beta5  beta6 ) (and beta9  beta10 )) (>= (+ (+  (* (- 1.00) mu6)  (* (- 1.00) mu12)  mu13  (* (- 1.00) mu16) )  m8 ) 0)))
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m7)  m15 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m8)  m15 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta10)  (>= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m7)  m15 )) 0)))
(assert (=>   beta10  (>= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m8)  m15 )) 0)))
;;; require min_a P(F(:,5),a) = 0 
(assert (= (+ (+  mu1  mu2  (* (- 1.00) mu3)  mu4  (* (- 1.00) mu5)  mu8  (* (- 1.00) mu9)  (* (- 1.00) mu10)  mu11 )  m15 ) 0))
;;; penalty P(F(:,6),a)) for x in F
;;; eliminate a(5)
(assert (<= (+ (+  mu6  (* (- 1.00) mu12)  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m16 ) 0))
(assert (<= (+ (+  mu6  (* (- 1.00) mu12)  (* (- 1.00) mu13)  mu16 )  m17 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  mu12  mu13  mu16 )  m16 ) 0))
(assert (<= (+ (+  (* (- 1.00) mu6)  mu12  mu13  (* (- 1.00) mu16) )  m17 ) 0))
;;; make message lower bounds tight
(assert (=>  (and (not beta11)  (not beta12) ) (>= (+ (+  mu6  (* (- 1.00) mu12)  (* (- 1.00) mu13)  (* (- 1.00) mu16) )  m16 ) 0)))
(assert (=>  (and (not beta11)  beta12 ) (>= (+ (+  mu6  (* (- 1.00) mu12)  (* (- 1.00) mu13)  mu16 )  m17 ) 0)))
(assert (=>  (and beta11  (not beta12) ) (>= (+ (+  (* (- 1.00) mu6)  mu12  mu13  mu16 )  m16 ) 0)))
(assert (=>  (and beta11  beta12 ) (>= (+ (+  (* (- 1.00) mu6)  mu12  mu13  (* (- 1.00) mu16) )  m17 ) 0)))
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  mu15 ) (+  (* (- 1.00) m16)  m18 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m17)  m18 )) 0))
;;; make message lower bounds tight
(assert (=>   (not beta12)  (>= (+ (+  mu7  mu14  mu15 ) (+  (* (- 1.00) m16)  m18 )) 0)))
(assert (=>   beta12  (>= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m17)  m18 )) 0)))
;;; require min_a P(F(:,6),a) = 0 
(assert (= (+ (+  mu1  mu2  mu3  (* (- 1.00) mu4)  (* (- 1.00) mu5)  (* (- 1.00) mu8)  (* (- 1.00) mu9)  (* (- 1.00) mu10)  (* (- 1.00) mu11) )  m18 ) 0))
;;; penalty P(Fbar(:,1),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m16)  m21 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  mu15 ) (+  (* (- 1.00) m17)  m21 )) 0))
;;; require min_a P(Fbar(:,1),a) >= 2.00
(assert (>= (+ (+  mu1  (* (- 1.00) mu2)  (* (- 1.00) mu3)  (* (- 1.00) mu4)  (* (- 1.00) mu5)  mu8  mu9  mu10  mu11 )  m21 ) 2.00))
;;; penalty P(Fbar(:,2),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m4)  m24 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  mu15 ) (+  (* (- 1.00) m5)  m24 )) 0))
;;; require min_a P(Fbar(:,2),a) >= 2.00
(assert (>= (+ (+  mu1  (* (- 1.00) mu2)  (* (- 1.00) mu3)  (* (- 1.00) mu4)  mu5  mu8  mu9  (* (- 1.00) mu10)  (* (- 1.00) mu11) )  m24 ) 2.00))
;;; penalty P(Fbar(:,3),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m7)  m27 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  mu15 ) (+  (* (- 1.00) m8)  m27 )) 0))
;;; require min_a P(Fbar(:,3),a) >= 2.00
(assert (>= (+ (+  mu1  (* (- 1.00) mu2)  (* (- 1.00) mu3)  mu4  (* (- 1.00) mu5)  (* (- 1.00) mu8)  (* (- 1.00) mu9)  mu10  mu11 )  m27 ) 2.00))
;;; penalty P(Fbar(:,4),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m16)  m30 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m17)  m30 )) 0))
;;; require min_a P(Fbar(:,4),a) >= 2.00
(assert (>= (+ (+  mu1  (* (- 1.00) mu2)  mu3  (* (- 1.00) mu4)  (* (- 1.00) mu5)  mu8  (* (- 1.00) mu9)  mu10  (* (- 1.00) mu11) )  m30 ) 2.00))
;;; penalty P(Fbar(:,5),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m1)  m33 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m2)  m33 )) 0))
;;; require min_a P(Fbar(:,5),a) >= 2.00
(assert (>= (+ (+  mu1  (* (- 1.00) mu2)  mu3  mu4  mu5  (* (- 1.00) mu8)  mu9  (* (- 1.00) mu10)  mu11 )  m33 ) 2.00))
;;; penalty P(Fbar(:,6),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m16)  m36 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m17)  m36 )) 0))
;;; require min_a P(Fbar(:,6),a) >= 2.00
(assert (>= (+ (+  mu1  mu2  (* (- 1.00) mu3)  (* (- 1.00) mu4)  (* (- 1.00) mu5)  (* (- 1.00) mu8)  mu9  (* (- 1.00) mu10)  mu11 )  m36 ) 2.00))
;;; penalty P(Fbar(:,7),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  (* (- 1.00) mu15) ) (+  (* (- 1.00) m1)  m39 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  mu15 ) (+  (* (- 1.00) m2)  m39 )) 0))
;;; require min_a P(Fbar(:,7),a) >= 2.00
(assert (>= (+ (+  mu1  mu2  (* (- 1.00) mu3)  mu4  mu5  mu8  (* (- 1.00) mu9)  mu10  (* (- 1.00) mu11) )  m39 ) 2.00))
;;; penalty P(Fbar(:,8),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  mu15 ) (+  (* (- 1.00) m4)  m42 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m5)  m42 )) 0))
;;; require min_a P(Fbar(:,8),a) >= 2.00
(assert (>= (+ (+  mu1  mu2  mu3  (* (- 1.00) mu4)  mu5  (* (- 1.00) mu8)  (* (- 1.00) mu9)  mu10  mu11 )  m42 ) 2.00))
;;; penalty P(Fbar(:,9),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  mu15 ) (+  (* (- 1.00) m7)  m45 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m8)  m45 )) 0))
;;; require min_a P(Fbar(:,9),a) >= 2.00
(assert (>= (+ (+  mu1  mu2  mu3  mu4  (* (- 1.00) mu5)  mu8  mu9  (* (- 1.00) mu10)  (* (- 1.00) mu11) )  m45 ) 2.00))
;;; penalty P(Fbar(:,10),a) for x not in F
;;; penalty for a(5) already defined
;;; eliminate a(6)
(assert (<= (+ (+  mu7  mu14  mu15 ) (+  (* (- 1.00) m1)  m48 )) 0))
(assert (<= (+ (+  (* (- 1.00) mu7)  (* (- 1.00) mu14)  (* (- 1.00) mu15) ) (+  (* (- 1.00) m2)  m48 )) 0))
;;; require min_a P(Fbar(:,10),a) >= 2.00
(assert (>= (+ (+  mu1  mu2  mu3  mu4  mu5  mu8  mu9  mu10  mu11 )  m48 ) 2.00))

(check-sat)
(get-model)

;;;# feasible solutions: 6, # infeasible solutions: 10
;;;# original beta variables: 12, # reduced beta variables: 10
;;;# original m variables: 48, # reduced m variables: 24
;;;# orignal message lower bound constraints 96, # reduced message lower bound constraints 48
;;;# orignal tight message lower bound constraints 36, # reduced tight message lower bound constraints 36
