#!/bin/sh
# run this script in the pysgen main folder to run maxsat experiments

mkdir -p reports_maxsat
for i in allbenchmarks_maxsat/*.wcnf;
do 
	BASEI=$(basename $i)
	line=$(head -n 1 $i)
	target=$(echo $line | cut -f3 -d' ')
	ubcsat -alg g2wsat -w -gtimeout 1 -runs 100000 -wtarget $target -r solution  -i $i > reports/g2wsat_${BASEI%.cnf}.txt;
done
